def fibonacci(num)
  return num if ( 0..1 ).include? num
  ( fibonacci( num - 1 ) + fibonacci( num - 2 ) )
end

def fibonacci_iterate
  num = 1
  while num <= 500  do
    puts fibonacci(num)
    num += 1
  end
end
fibonacci_iterate
