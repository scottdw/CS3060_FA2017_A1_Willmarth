class Shape
  color = 'red'
  filled = true

  def initialize()
    @color = ''
    @filled = false
  end

  def getColor()
    @color
  end

  def setColor=(c)
    @color = c
  end

  def isFilled()
    @filled
  end

  def setFilled=(f)
    @filled = f
  end

  def toString()
  end
end

class Circle < Shape
  radius = 1.0

  def initialize()
    @radius = 0.0
    @color = ''
    @filled = false
  end


  def setRadius=(r)
    @radius = r
  end

  def getRadius()
    @radius
  end

end

class Rectangle < Shape
  width = 1.0
  length = 1.0

  def initialize()
    @width = 0.0
    @length = 0.0
    @color = ''
    @filled = false
  end


  def setWidth=(w)
    @width = w
  end

  def getWidth()
    @width
  end

  def setLength=(l)
    @length = l
  end

  def getLength()
    @length
  end

  def get_Area()
    area = width * length
    @area
  end

  def get_Perimeter()
    perimeter = width + length
    @perimeter
  end

end

class Square < Rectangle
  side = 1.0

  def initialize()
    @side = 0.0
    @color = ''
    @filled = false
  end


  def setSide=(s)
    @side = s
  end

  def getSide()
    @side
  end


end

triangle = Shape.new()
triangle.setColor ='yellow'
triangle.setFilled = false
octagon = Shape.new()
octagon.setColor ='orange'
octagon.setFilled = true
oval = Circle.new()
oval.setRadius = 3.0
oval.setColor = 'brown'
oval.setFilled = true
oval.isFilled
rect = Rectangle.new()
rect.setWidth = 4.0
rect.setLength = 5.0
sq = Square.new()
sq.setColor = 'purple'

puts triangle.getColor
puts octagon.isFilled
puts oval.getRadius
puts rect.getWidth
puts rect.getLength
puts sq.getSide
sq.setSide = 4.5
puts sq.getSide
