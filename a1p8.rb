def going_ham
  file = File.open("filename", "r")
  while (line = file.gets)
    temp_string = line
    string_one = temp_string.match(',').pre_match
    string_two = temp_string.match(',').post_match

    ham_count = 0
    string_length = string_one.length
    for i in 0...string_length
      if string_one[i] != string_two[i]
        ham_count += 1
      end
    end
    file << string_one + ':' + string_two + ':' + ham_count.to_s
  end
end
