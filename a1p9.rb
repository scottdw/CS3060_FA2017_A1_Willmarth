class CsvReader
  def read_csv
    file = File.open("filename.csv", "r")
    while (line = file.gets)
      temp_string = line
      while temp_string.include? ','
        string_one = temp_string.match(',').pre_match
        string_two = temp_string.match(',').post_match
        puts string_one
        temp_string = string_two
      end
      puts temp_string
    end
  end

end

csVRow = CsvReader.new.read_csv
