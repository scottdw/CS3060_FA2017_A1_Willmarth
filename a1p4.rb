def array_select
  n = 100
  the_array = Array.new(n) {rand(1...n)}
  length = the_array.size - 1

  length.times do |x|
    minimum = x

    (x+1).upto(length) do |y|
      minimum = y if the_array[y] < the_array[minimum]
    end

    the_array[x], the_array[minimum] = the_array[minimum], the_array[x] if minimum != x
  end

  puts the_array.join(' ')
end

array_select
