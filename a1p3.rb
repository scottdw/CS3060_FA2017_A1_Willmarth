def array_sort
  the_array = Array.new(100) {rand(1...100)}
  the_array.sort!
  puts 'Sorted Array'
  puts the_array.join(' ')
  the_array.reverse!
  puts 'Reverse Array'
  puts the_array.join(' ')
  puts 'Odd Index Only Array'
  puts the_array.values_at(* the_array.each_index.select {|i| i.odd?}).join(' ')
end
array_sort
